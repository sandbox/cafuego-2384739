<?php
/**
 * @file
 */

/**
 * Implements hook_views_data().
 */
function views_date_range_rewrite_field_views_data() {
  $data['views']['views_date_range_rewrite_field'] = array(
    'title' => t('Date range rewrite'),
    'group' => t('Date'),
    'help' => t('Output text based on a date field with start and end values.'),
    'field' => array(
      'help' => t('Output text based on a date field with start and end values.'),
      'handler' => 'views_date_range_rewrite_field',
    ),
  );

  return $data;
}
